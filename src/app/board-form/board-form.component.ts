import { Component, OnInit } from '@angular/core';
import { Column, ColumnsService } from '../shared/column.service';

@Component({
  selector: 'app-board-form',
  templateUrl: './board-form.component.html',
  styleUrls: ['./board-form.component.scss'],
})
export class BoardFormComponent implements OnInit {
  title: string;

  constructor(public columnsService: ColumnsService) {}

  ngOnInit(): void {}

  addColumn() {
    const column: Column = {
      title: this.title || 'Test Column',
      id: Date.now(),
      date: new Date(),
    };
    this.columnsService.addColumn(column);
    this.title = '';
  }
}
