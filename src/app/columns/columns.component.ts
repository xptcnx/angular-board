import { Component, OnInit } from '@angular/core';
import { ColumnsService } from '../shared/column.service';

@Component({
  selector: 'app-columns',
  templateUrl: './columns.component.html',
  styleUrls: ['./columns.component.scss']
})
export class ColumnsComponent implements OnInit {

  constructor(public columnsService: ColumnsService) { }

  ngOnInit(): void {
  }

  removeId(id: number) {
    this.columnsService.removeColumn(id);
  }
}
