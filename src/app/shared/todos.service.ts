import { Injectable } from '@angular/core';

export interface Todo {
  id: number;
  title: string;
  date?: any;
}

@Injectable({ providedIn: 'root' })
export class TodosService {
  public todos: Todo[] = [];

  constructor() {}

  removeTodo(id: number) {
    this.todos = this.todos.filter((todo) => todo.id !== id);
  }

  addTodo(todo: Todo) {
    this.todos.push(todo);
  }
}
