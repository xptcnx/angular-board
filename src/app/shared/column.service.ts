import { Injectable } from '@angular/core';

export interface Column {
  id: number;
  title: string;
  date?: any;
}

@Injectable({ providedIn: 'root' })
export class ColumnsService {
  public columns: Column[] = [];

  constructor() {}


  removeColumn(id: number) {
    this.columns = this.columns.filter((column) => column.id !== id);
  }

  addColumn(column: Column) {
    this.columns.push(column);
  }
}

